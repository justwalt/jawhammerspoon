HYPER = {"cmd", "alt", "ctrl", "shift"}
target_window_1 = nil
target_window_2 = nil
previous_window = nil
caffeine_active = false

-- Reload config --
hs.hotkey.bind(HYPER, "forwarddelete", function()
    hs.reload()
end)

-- Get mouse position --
hs.hotkey.bind(HYPER, "Y", function()
    local posi = hs.mouse.getAbsolutePosition()
    alert_and_clear_others(posi.x .. ", " .. posi.y)
end)

function center_of_window (desired_window)
    local focused_winframe = desired_window:frame()
    local point_obj = {}

    point_obj["x"] = focused_winframe.x + math.floor(focused_winframe.w / 2)
    point_obj["y"] = focused_winframe.y + math.floor(focused_winframe.h / 2)

    return point_obj
end

function move_mouse_to_center_of_window (desired_window)
    hs.mouse.setAbsolutePosition(center_of_window(desired_window))
end

function alert_and_clear_others (alert_text)
    hs.alert.closeAll()
    hs.alert.show(alert_text)
end

-- Opening applications --
function open_app (name_str)
    previous_window = hs.window.focusedWindow()
    hs.application.open(name_str)
end

-- Activating applications --
function activate_app (name_str)
    previous_window = hs.window.focusedWindow()
    local app = hs.application.get(name_str)
    app:activate()
    move_mouse_to_center_of_window(app:mainWindow())
end

-- Move mouse cursor to the center of the focused window
hs.hotkey.bind(HYPER, "space", function()
    local focused_window = hs.window.focusedWindow()
    move_mouse_to_center_of_window(focused_window)
end)

-- Window resizing left and right --
hs.hotkey.bind(HYPER, "Z", function()
    -- Half screen, left side
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()
    
    focused_winframe.x = workspace_frame.x
    focused_winframe.y = workspace_frame.y
    focused_winframe.w = workspace_frame.w / 2
    focused_winframe.h = workspace_frame.h
    
    focused_window:setFrame(focused_winframe)
end)

hs.hotkey.bind(HYPER, "V", function()
    -- Half screen, right side
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()
    
    focused_winframe.x = workspace_frame.x + (workspace_frame.w / 2)
    focused_winframe.y = workspace_frame.y
    focused_winframe.w = workspace_frame.w / 2
    focused_winframe.h = workspace_frame.h
    
    focused_window:setFrame(focused_winframe)
end)

hs.hotkey.bind(HYPER, "X", function()
    -- 70% screen, left side
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()
    
    -- SCALAR = (workspace_frame.w / 4400.0) + 0.9682
    -- SCALAR = (workspace_frame.w / 3520.0) + 0.8727
    SCALAR = 1.5
    
    focused_winframe.x = workspace_frame.x
    focused_winframe.y = workspace_frame.y
    focused_winframe.w = math.floor(workspace_frame.w / SCALAR)
    focused_winframe.h = workspace_frame.h
    
    focused_window:setFrame(focused_winframe)
end)

hs.hotkey.bind(HYPER, "C", function()
    -- 70% screen, right side
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()

    -- SCALAR = (workspace_frame.w / 4400.0) + 0.9682
    -- SCALAR = (workspace_frame.w / 3520.0) + 0.8727
    SCALAR = 1.5
    
    focused_winframe.x = workspace_frame.x + (workspace_frame.w - math.floor(workspace_frame.w / SCALAR))
    focused_winframe.y = workspace_frame.y
    focused_winframe.w = math.floor(workspace_frame.w / SCALAR)
    focused_winframe.h = workspace_frame.h
    
    focused_window:setFrame(focused_winframe)
end)

hs.hotkey.bind(HYPER, "Home", function()
    -- 25% quadrant style
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()

    local ws_right_edge = workspace_frame.w + workspace_frame.x
    local ws_bottom_edge = workspace_frame.h + workspace_frame.y

    focused_winframe.w = workspace_frame.w / 2
    if focused_window:application():title() == "iTerm2" then
        focused_winframe.h = math.floor(workspace_frame.h / 2) - 10 + ((math.floor(workspace_frame.h / 2) - 10) % 18)
    else
        focused_winframe.h = workspace_frame.h / 2
    end

    local new_right_edge = focused_winframe.x + workspace_frame.w / 2
    local new_bottom_edge = focused_winframe.y + focused_winframe.h

    if new_right_edge > ws_right_edge then
        focused_winframe.x = focused_winframe.x + (ws_right_edge - new_right_edge)
    end

    if new_bottom_edge > ws_bottom_edge then
        focused_winframe.y = (focused_winframe.y + (ws_bottom_edge - new_bottom_edge))
    end

    focused_window:setFrame(focused_winframe)
end)

hs.hotkey.bind(HYPER, "End", function()
    -- 33% screen
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()

    local ws_right_edge = workspace_frame.w + workspace_frame.x
    local ws_bottom_edge = workspace_frame.h + workspace_frame.y

    local new_right_edge = focused_winframe.x + workspace_frame.w / 3
    local new_bottom_edge = focused_winframe.y + workspace_frame.h

    if new_right_edge > ws_right_edge then
        focused_winframe.x = focused_winframe.x + (ws_right_edge - new_right_edge)
    end

    if new_bottom_edge > ws_bottom_edge then
        focused_winframe.y = (focused_winframe.y + (ws_bottom_edge - new_bottom_edge))
    end

    focused_winframe.w = workspace_frame.w / 3
    focused_winframe.h = workspace_frame.h
    
    focused_window:setFrame(focused_winframe)
end)

function move_win_to_corner (window_to_move)
    -- move window to the nearest corner, keeping dimensions
    local focused_winframe = window_to_move:frame()
    local workspace_screen = window_to_move:screen()
    local workspace_frame = workspace_screen:frame()

    -- find corners of active window
    local winframe_corner_NW = {}
    winframe_corner_NW.x = focused_winframe.x
    winframe_corner_NW.y = focused_winframe.y
    local winframe_corner_NE = {}
    winframe_corner_NE.x = focused_winframe.x + focused_winframe.w
    winframe_corner_NE.y = focused_winframe.y
    local winframe_corner_SW = {}
    winframe_corner_SW.x = focused_winframe.x
    winframe_corner_SW.y = focused_winframe.y + focused_winframe.h
    local winframe_corner_SE = {}
    winframe_corner_SE.x = focused_winframe.x + focused_winframe.w
    winframe_corner_SE.y = focused_winframe.y + focused_winframe.h

    -- find corners of the active window's desktop
    local workspace_corner_NW = {}
    workspace_corner_NW.x = workspace_frame.x
    workspace_corner_NW.y = workspace_frame.y
    local workspace_corner_NE = {}
    workspace_corner_NE.x = workspace_frame.x + workspace_frame.w
    workspace_corner_NE.y = workspace_frame.y
    local workspace_corner_SW = {}
    workspace_corner_SW.x = workspace_frame.x
    workspace_corner_SW.y = workspace_frame.y + workspace_frame.h
    local workspace_corner_SE = {}
    workspace_corner_SE.x = workspace_frame.x + workspace_frame.w
    workspace_corner_SE.y = workspace_frame.y + workspace_frame.h

    -- find the distance between each corresponding pair of corners
    local distance_NW = math.sqrt((winframe_corner_NW.x - workspace_corner_NW.x)^2 + (winframe_corner_NW.y - workspace_corner_NW.y)^2)
    local distance_NE = math.sqrt((winframe_corner_NE.x - workspace_corner_NE.x)^2 + (winframe_corner_NE.y - workspace_corner_NE.y)^2)
    local distance_SW = math.sqrt((winframe_corner_SW.x - workspace_corner_SW.x)^2 + (winframe_corner_SW.y - workspace_corner_SW.y)^2)
    local distance_SE = math.sqrt((winframe_corner_SE.x - workspace_corner_SE.x)^2 + (winframe_corner_SE.y - workspace_corner_SE.y)^2)

    -- -- debug printing
    -- hs.alert.show(
    --     "NW: " .. distance_NW .. "\n" ..
    --     "NE: " .. distance_NE .. "\n" ..
    --     "SW: " .. distance_SW .. "\n" ..
    --     "SE: " .. distance_SE
    -- )

    -- decide which corner gets placement
    if distance_NW < distance_NE then
        -- left side of screen
        if distance_NW < distance_SW then
            -- upper left (NW)
            focused_winframe.x = workspace_corner_NW.x
            focused_winframe.y = workspace_corner_NW.y
        else
            -- lower left (SW)
            focused_winframe.x = workspace_corner_SW.x
            focused_winframe.y = workspace_corner_SW.y - focused_winframe.h
        end
    else
        -- right side of screen
        if distance_NE < distance_SE then
            -- upper right (NE)
            focused_winframe.x = workspace_corner_NE.x - focused_winframe.w
            focused_winframe.y = workspace_corner_NE.y
        else
            -- lower right (SE)
            focused_winframe.x = workspace_corner_SE.x - focused_winframe.w
            focused_winframe.y = workspace_corner_SE.y - focused_winframe.h
        end
    end

    window_to_move:setFrame(focused_winframe)
end

function maximize_window (window_to_maximize)
    local focused_winframe = window_to_maximize:frame()
    local workspace_screen = window_to_maximize:screen()
    local workspace_frame = workspace_screen:frame()

    focused_winframe.x = workspace_frame.x
    focused_winframe.y = workspace_frame.y
    focused_winframe.w = workspace_frame.w
    focused_winframe.h = workspace_frame.h

    window_to_maximize:setFrame(focused_winframe)
end

hs.hotkey.bind(HYPER, "B", function()
    local focused_window = hs.window.focusedWindow()
    -- move_win_to_corner(focused_window)
    maximize_window(focused_window)
end)

function move_window_to_edge (window_to_move, direction)
    -- move window to the specified edge, keeping dimensions
    local focused_winframe = window_to_move:frame()
    local workspace_screen = window_to_move:screen()
    local workspace_frame = workspace_screen:frame()

    -- find edge of active desktop
    local workspace_side_d = workspace_frame.y + workspace_frame.h
    local workspace_side_r = workspace_frame.x + workspace_frame.w

    if direction == "up" then
        focused_winframe.y = workspace_frame.y
    end
    if direction == "down" then
        focused_winframe.y = workspace_side_d - focused_winframe.h
    end
    if direction == "left" then
        focused_winframe.x = workspace_frame.x
    end
    if direction == "right" then
        focused_winframe.x = workspace_side_r - focused_winframe.w
    end

    window_to_move:setFrame(focused_winframe)
end

hs.hotkey.bind(HYPER, "Up", function()
    local focused_window = hs.window.focusedWindow()
    move_window_to_edge(focused_window, "up")
end)

hs.hotkey.bind(HYPER, "Down", function()
    local focused_window = hs.window.focusedWindow()
    move_window_to_edge(focused_window, "down")
end)

hs.hotkey.bind(HYPER, "Left", function()
    local focused_window = hs.window.focusedWindow()
    move_window_to_edge(focused_window, "left")
end)

hs.hotkey.bind(HYPER, "Right", function()
    local focused_window = hs.window.focusedWindow()
    move_window_to_edge(focused_window, "right")
end)

function move_window_to_mouse (desired_window)
    local focused_winframe = desired_window:frame()
    local mouse_pos = hs.mouse.absolutePosition()

    focused_winframe.x = mouse_pos.x - math.floor(0.5 * focused_winframe.w)
    focused_winframe.y = mouse_pos.y - math.floor(0.5 * focused_winframe.h)

    desired_window:setFrame(focused_winframe)
end

hs.hotkey.bind(HYPER, "F", function()
    local focused_window = hs.window.focusedWindow()
    move_window_to_mouse(focused_window)
end)

function set_win_size_to_dims (desired_window, win_w, win_h)
    local focused_winframe = desired_window:frame()
    local workspace_screen = desired_window:screen()
    local workspace_frame = workspace_screen:frame()

    local ws_right_edge = workspace_frame.w + workspace_frame.x
    local ws_bottom_edge = workspace_frame.h + workspace_frame.y

    local new_right_edge = focused_winframe.x + win_w
    local new_bottom_edge = focused_winframe.y + win_h

    if new_right_edge > ws_right_edge then
        focused_winframe.x = focused_winframe.x + (ws_right_edge - new_right_edge)
    end

    if new_bottom_edge > ws_bottom_edge then
        focused_winframe.y = (focused_winframe.y + (ws_bottom_edge - new_bottom_edge))
    end

    focused_winframe.w = win_w
    focused_winframe.h = win_h
    
    desired_window:setFrame(focused_winframe)
end

hs.hotkey.bind(HYPER, "N", function()
    -- Resize window to more manageable size
    local focused_window = hs.window.focusedWindow()

    local win_w
    if focused_window:application():title() == "iTerm2" then
        win_w = 1063.0
    else
        win_w = 1078.0
    end
    
    local win_h = 875.0

    set_win_size_to_dims(focused_window, win_w, win_h)
end)

hs.hotkey.bind(HYPER, "M", function()
    -- Resize window to more manageable size
    local focused_window = hs.window.focusedWindow()
    
    local win_w = 1420.0
    local win_h = 1220.0

    set_win_size_to_dims(focused_window, win_w, win_h)
end)

function new_iterm_window ()
    previous_window = hs.window.focusedWindow()
    local iterm = hs.application.get("iTerm2")
    if iterm ~= nil then
        -- The reason that the menu item would occasionally not trigger is because
        -- of oddness from fullscreen apps. If a window is fullscreened and being
        -- displayed on the furthest left monitor, then the menu item will not
        -- trigger.
        result = iterm:selectMenuItem({"Shell", "New Window"})
        if result == nil then
            -- print some debug stuff
            alert_and_clear_others("Debug info printed to console.")

            local newwin_item = iterm:getMenuItems()
            if newwin_item ~= nil then
                print("Menu items:")

                print(hs.inspect(newwin_item))
            else
                print("Menu returned nil..")
            end

            iterm:selectMenuItem("New Window", true)
        end
    else
        hs.application.open("iTerm")
    end
end

hs.hotkey.bind({"cmd"}, "F19", function()
    new_iterm_window()
end)

hs.hotkey.bind(HYPER, "`", function()
    new_iterm_window()
end)

-- hs.hotkey.bind({"cmd"}, "F19", function()
--     previous_window = hs.window.focusedWindow()
--     local alacritty = hs.application.get("Alacritty")
--     if alacritty ~= nil then
--         alacritty:activate()
--         hs.eventtap.keyStroke({"cmd"}, "N")
--     else
--         hs.application.open("Alacritty")
--     end
-- end)

hs.hotkey.bind({"cmd", "ctrl"}, "F19", function() activate_app("iTerm2") end)
hs.hotkey.bind({"cmd"}, "F14", function()
    local focused_window = hs.window.focusedWindow()
    if focused_window:application():title() == "Mail" then
        activate_app("Calendar")
    else
        activate_app("Mail")
    end
end)
hs.hotkey.bind({"cmd"}, "F15", function() activate_app("Spotify") end)
hs.hotkey.bind({"cmd"}, "F16", function() activate_app("Firefox") end)
hs.hotkey.bind({"cmd"}, "F17", function() activate_app("Google Chat") end)
-- hs.hotkey.bind({"cmd"}, "F18", function() activate_app("Sublime Text") end)

hs.hotkey.bind(HYPER, "F14", function() open_app("Mail") end)
hs.hotkey.bind(HYPER, "F15", function() open_app("Spotify") end)
-- hs.hotkey.bind(HYPER, "F16", function() open_app("Firefox") end)
hs.hotkey.bind(HYPER, "F16", function()
    previous_window = hs.window.focusedWindow()
    local firefox = hs.application.get("Firefox")
    if firefox ~= nil then
        result = firefox:selectMenuItem("New Window", true)
        hs.timer.usleep(500)
        local new_ff = hs.application.get("Firefox")
        new_ff:activate()
        -- move_window_to_mouse(hs.window.focusedWindow())
        if result == nil then
            alert_and_clear_others("Debug info printed to console.")
            local newwin_item = firefox:getMenuItems()
            if newwin_item ~= nil then
                print("Menu items:")
                print(hs.inspect(newwin_item))
            else
                print("Menu returned nil..")
            end
        end
    else
        alert_and_clear_others("Couldn't open Firefox..?")
    end
end)
hs.hotkey.bind(HYPER, "F17", function() open_app("Google Chat") end)
-- hs.hotkey.bind(HYPER, "F18", function() open_app("Sublime Text") end)

-- Select a specific window as the target for the F18 key
hs.hotkey.bind(HYPER, "F18", function()
    local new_target_window = hs.window.focusedWindow()
    if target_window_1 == new_target_window then
        target_window_1 = nil
        alert_and_clear_others("Cleared target window 1")
    else
        target_window_1 = new_target_window
        alert_and_clear_others("Target window 1 set: " .. target_window_1:title())
    end
end)

-- Select a specific window as the target for the F13 key
hs.hotkey.bind(HYPER, "F13", function()
    local new_target_window = hs.window.focusedWindow()
    if target_window_2 == new_target_window then
        target_window_2 = nil
        alert_and_clear_others("Cleared target window 2")
    else
        target_window_2 = new_target_window
        alert_and_clear_others("Target window 2 set: " .. target_window_2:title())
    end
end)

-- Activate the 1st target window
hs.hotkey.bind({"cmd"}, "F18", function()
    if target_window_1 ~= nil then
        previous_window = hs.window.focusedWindow()
        target_window_1:focus()
        target_window_1:becomeMain()
        move_mouse_to_center_of_window(target_window_1)
    else
        alert_and_clear_others("No target window [1]")
    end
end)

-- Activate the 2nd target window
hs.hotkey.bind({"cmd"}, "F13", function()
    if target_window_2 ~= nil then
        previous_window = hs.window.focusedWindow()
        target_window_2:focus()
        target_window_2:becomeMain()
        move_mouse_to_center_of_window(target_window_2)
    else
        alert_and_clear_others("No target window [2]")
    end
end)

hs.hotkey.bind(HYPER, "tab", function()
    -- switch back to the window used last before using another hotkey
    -- more or less a jerry rigged alt-tab, but can be used on the same
    -- window from anywhere if you don't use another hotkey
    local placeholder_previous_window = hs.window.focusedWindow()
    if previous_window ~= nil then
        previous_window:focus()
        previous_window:becomeMain()
        move_mouse_to_center_of_window(previous_window)
        previous_window = placeholder_previous_window
    else
        alert_and_clear_others("Previous window isn't set")
    end
end)

hs.hotkey.bind(HYPER, "A", function()
    local iterm = hs.application.get("iTerm2")
    if iterm ~= nil then
        iterm:selectMenuItem({"Profiles", "py3interp"})
        -- iterm:selectMenuItem({"Profiles", "py3interp-light"})
    else
        hs.application.open("iTerm"):selectMenuItem({"Profiles", "py3interp"})
        -- hs.application.open("iTerm"):selectMenuItem({"Profiles", "py3interp-light"})
    end
end)

-- open an iterm window in the current finder directory
hs.hotkey.bind({"cmd", "alt"}, "F19", function()
    previous_window = hs.window.focusedWindow()
    local focused_application = hs.window.focusedWindow():application()
    local finder = hs.application.get("Finder")
    if focused_application ~= finder then
        alert_and_clear_others("Can only open terminal from directory for finder")
        return
    end
    if finder ~= nil then
        result = finder:selectMenuItem("New iTerm2 Window Here", true)
        if result == nil then
            alert_and_clear_others("Debug info printed to console.")
            local newwin_item = finder:getMenuItems()
            if newwin_item ~= nil then
                print("Menu items:")
                print(hs.inspect(newwin_item))
            else
                print("Menu returned nil..")
            end
        end
    else
        alert_and_clear_others("Couldn't open finder??")
    end
end)

-- toggle mouse reporting in a tmux terminal session so you can highlight in mouse-mode tmux
hs.hotkey.bind(HYPER, "F19", function()
    local iterm = hs.application.get("iTerm2")
    if iterm ~= nil then
        result = iterm:selectMenuItem("Mouse Reporting", true)
        if result == nil then
            alert_and_clear_others("Debug info printed to console.")
            local newwin_item = iterm:getMenuItems()
            if newwin_item ~= nil then
                print("Menu items:")
                print(hs.inspect(newwin_item))
            else
                print("Menu returned nil..")
            end
        end
    else
        alert_and_clear_others("Couldn't find iTerm2 instance")
    end
end)

-- open a new finder window instead of right clicking the dock
hs.hotkey.bind("cmd", "F20", function()
    previous_window = hs.window.focusedWindow()
    local finder = hs.application.get("Finder")
    if finder ~= nil then
        result = finder:selectMenuItem("New Finder Window", true)
        if result == nil then
            alert_and_clear_others("Debug info printed to console.")
            local newwin_item = finder:getMenuItems()
            if newwin_item ~= nil then
                print("Menu items:")
                print(hs.inspect(newwin_item))
            else
                print("Menu returned nil..")
            end
        end
    else
        alert_and_clear_others("Couldn't open finder??")
    end
end)

hs.hotkey.bind(HYPER, "Return", function()
    if caffeine_active == true then
        Caffeine:stop()
        caffeine_active = false
        alert_and_clear_others("Caffeine off")
    else
        Caffeine:start()
        caffeine_active = true
        alert_and_clear_others("Caffeine on")
    end
end)

-- #< TEST FUNCTIONS >#

-- hs.hotkey.bind("cmd", "F21", function()
--     alert_and_clear_others("placeholder")
-- end)

hs.hotkey.bind(HYPER, "T", function()
    local focused_window = hs.window.focusedWindow()
    local focused_winframe = focused_window:frame()
    local workspace_screen = focused_window:screen()
    local workspace_frame = workspace_screen:frame()

    local mouse_pos = hs.mouse.getRelativePosition()
    
    alert_and_clear_others(
        "Name: " .. focused_window:application():name() .. "\n" ..
        "Title: " .. focused_window:application():title() .. "\n\n" ..
        "Loca: (" .. focused_winframe.x .. ", " .. focused_winframe.y .. ")\n" ..
        "Dims: " .. focused_winframe.w .. "x" .. focused_winframe.h .. "\n" ..
        "WS Loca: (" .. workspace_frame.x .. ", " .. workspace_frame.y .. ")\n" ..
        "WS Dims: " .. workspace_frame.w .. "x" .. workspace_frame.h .. ")\n\n" ..
        "Mouse pos: (" .. mouse_pos.x .. ", " .. mouse_pos.y .. ")"
    )
end)

-- #< END TEST FUNCTIONS >#

alert_and_clear_others("Hammerspoon active")
